package com.wsr.animatewatercounter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.gms.wearable.*
import com.wsr.animatewatercounter.databinding.ActivityMainBinding
import com.wsr.water.GradientParser
import com.wsr.water.OnAnimateWater
import com.wsr.water.WaterCounterView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.ceil

class MainActivity : AppCompatActivity(), DataClient.OnDataChangedListener {

    private val maxWater = 3000
    private val minWater = 0
    private val stepWater = 250
    private val countSteps = maxWater / stepWater

    private lateinit var colors: List<Color>

    private lateinit var binding: ActivityMainBinding

    private lateinit var client: DataClient
    private lateinit var putDataMapRequest: PutDataMapRequest

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.actionBar)))

        left_counter.title = "Осталось:"
        left_counter.maxCount = maxWater
        left_counter.minCount = minWater
        left_counter.count = maxWater

        drink_counter.title = "Выпито:"
        drink_counter.maxCount = maxWater
        drink_counter.minCount = minWater
        drink_counter.count = minWater

        water.onAnimateWater = object: OnAnimateWater {
            override fun onStartAnimate() {
                add_drink_water.isEnabled = false
            }

            override fun onEndAnimate() {
                add_drink_water.isEnabled = true
                refreshColorTextWaterCounter(drink_counter)
                refreshColorTextWaterCounter(left_counter)
            }
        }
        val stepPx = calcPxStepWater()

        colors = GradientParser(this, R.drawable.gradient).parseColors(countSteps)

        add_drink_water.setOnClickListener {
            drink_counter.count += stepWater
            left_counter.count -= stepWater
            val nowStep = drink_counter.count / stepWater - 1
            val color = colors[nowStep]
            water.startAnim(stepPx, color.toArgb())

            pushRequestToWear()
        }

        client = Wearable.getDataClient(this)
        putDataMapRequest = PutDataMapRequest.create("/counter")
    }

    private fun calcPxStepWater(): Int{
        val space = windowManager.defaultDisplay.height - (supportActionBar?.height ?: 0)
        return ceil(space / countSteps.toFloat()).toInt()
    }

    private fun refreshColorTextWaterCounter(waterCounter: WaterCounterView){
        val waterLineY = water.getYLineWater()
        val white = ContextCompat.getColor(this@MainActivity, R.color.white)
        if (waterCounter.checkCollisionCounterY(waterLineY))
            waterCounter.setColorTextCounter(white)
        if (waterCounter.checkCollisionTitleY(waterLineY))
            waterCounter.setColorTextTitle(white)
    }

    private fun pushRequestToWear(){
        val request = putDataMapRequest.run {
            dataMap.apply {
                putInt("drink_counter", binding.drinkCounter.count)
            }
            setUrgent()
            asPutDataRequest()
        }
        client.putDataItem(request)
        Log.e("phone send", binding.drinkCounter.count.toString())
    }

    override fun onResume() {
        super.onResume()
        client.addListener(this)
    }

    override fun onPause() {
        super.onPause()
        client.removeListener(this)
    }

    override fun onDataChanged(p0: DataEventBuffer) {
        p0.forEach { event ->
            if (event.type == DataEvent.TYPE_CHANGED) {
                val item = event.dataItem
                val path = item.uri.path ?: return@forEach

                if (path == "/counter") {
                    val dataMap = DataMapItem.fromDataItem(item).dataMap
                    val drinkCount = dataMap.getInt("drink_counter")
                    binding.drinkCounter.count = drinkCount
                    Log.e("phone task catch", drinkCount.toString())
                }
            }
        }
    }
}