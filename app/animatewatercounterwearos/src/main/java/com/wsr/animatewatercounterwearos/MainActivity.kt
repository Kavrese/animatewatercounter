package com.wsr.animatewatercounterwearos

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.android.gms.wearable.*
import com.wsr.animatewatercounterwearos.databinding.ActivityMainBinding
import com.wsr.water.GradientParser
import com.wsr.water.OnAnimateWater
import com.wsr.water.WaterCounterView
import kotlin.math.ceil

class MainActivity : Activity(), DataClient.OnDataChangedListener {

    private val maxWater = 3000
    private val minWater = 0
    private val stepWater = 250
    private val countSteps = maxWater / stepWater

    private lateinit var colors: List<Color>

    private lateinit var binding: ActivityMainBinding

    private lateinit var client: DataClient
    private lateinit var putDataMapRequest: PutDataMapRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.leftCounter.apply {
            title = "Осталось:"
            maxCount = maxWater
            minCount = minWater
            count = maxWater
        }

        binding.drinkCounter.apply {
            title = "Выпито:"
            maxCount = maxWater
            minCount = minWater
            count = minWater
        }


        binding.water.onAnimateWater = object: OnAnimateWater {
            override fun onStartAnimate() {
                binding.addDrinkWater.isEnabled = false
            }

            override fun onEndAnimate() {
                binding.addDrinkWater.isEnabled = true
                refreshColorTextWaterCounter(binding.drinkCounter)
                refreshColorTextWaterCounter(binding.leftCounter)
            }
        }
        val stepPx = calcPxStepWater()

        colors = GradientParser(this, R.drawable.gradient).parseColors(countSteps)

        binding.addDrinkWater.setOnClickListener {
            binding.drinkCounter.count += stepWater
            binding.leftCounter.count -= stepWater
            val nowStep = binding.drinkCounter.count / stepWater - 1
            val color = colors[nowStep]
            binding.water.startAnim(stepPx, color.toArgb())

            pushRequestToWear()
        }

        client = Wearable.getDataClient(this@MainActivity)
        putDataMapRequest = PutDataMapRequest.create("/counter")

    }

    private fun calcPxStepWater(): Int{
        val space = windowManager.defaultDisplay.height - binding.addDrinkWater.layoutParams.height
        return ceil(space / countSteps.toFloat()).toInt()
    }

    private fun refreshColorTextWaterCounter(waterCounter: WaterCounterView){
        val waterLineY = binding.water.getYLineWater()
        val white = ContextCompat.getColor(this@MainActivity, R.color.white)
        if (waterCounter.checkCollisionCounterY(waterLineY))
            waterCounter.setColorTextCounter(white)
        if (waterCounter.checkCollisionTitleY(waterLineY))
            waterCounter.setColorTextTitle(white)
    }

    private fun pushRequestToWear(){
        val request = putDataMapRequest.run {
            dataMap.apply {
                putInt("drink_counter", binding.drinkCounter.count)
            }
            setUrgent()
            asPutDataRequest()
        }
        client.putDataItem(request)
        Log.e("wear send", binding.drinkCounter.count.toString())
    }

    override fun onDataChanged(p0: DataEventBuffer) {
        p0.forEach { event ->
            if (event.type == DataEvent.TYPE_CHANGED) {
                val item = event.dataItem
                val path = item.uri.path ?: return@forEach

                if (path == "/counter") {
                    val dataMap = DataMapItem.fromDataItem(item).dataMap
                    val drinkCount = dataMap.getInt("drink_counter")
                    binding.drinkCounter.count = drinkCount
                    Log.e("wear task catch", drinkCount.toString())
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        client.addListener(this)
    }

    override fun onPause() {
        super.onPause()
        client.removeListener(this)
    }
}