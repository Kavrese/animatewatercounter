package com.wsr.water

import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.layout_level_water.view.*

class LevelWaterView: LinearLayout {
    constructor(context: Context): super(context)
    constructor(context: Context, attr: AttributeSet): super(context, attr)
    constructor(context: Context, attr: AttributeSet, defStyle: Int): super(context, attr, defStyle)

    var duration: Long = 500
    var nowColor: Int? = null
    var onAnimateWater: OnAnimateWater? = null

    init {
        inflate(context, R.layout.layout_level_water, this)
    }

    fun startAnim(px: Int, color: Int){
        val newHeightPx = level_water.height + px

        val set = AnimatorSet()
        set.play(generateHeightValueAnimator(newHeightPx))
            .with(generateColorValueAnimator(color))
        set.duration = duration

        set.start()

        onAnimateWater?.onStartAnimate()
        nowColor = color
    }

    @SuppressLint("Recycle")
    private fun generateColorValueAnimator(colorTo: Int): ValueAnimator{
        val colorFrom = nowColor ?: colorTo
        val colorValueAnimator = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
        colorValueAnimator.addUpdateListener {
            val animatedValue = colorValueAnimator.animatedValue as Int
            level_water.setBackgroundColor(animatedValue)
        }
        return colorValueAnimator
    }

    @SuppressLint("Recycle")
    private fun generateHeightValueAnimator(newHeightPx: Int): ValueAnimator {
        val heightValueAnimator = ValueAnimator.ofInt(level_water.measuredHeight, newHeightPx)
        heightValueAnimator.addUpdateListener {
            val animatedValue = heightValueAnimator.animatedValue as Int
            val layoutParams = level_water.layoutParams
            layoutParams.height = animatedValue
            level_water.layoutParams = layoutParams
            if (newHeightPx == animatedValue)
                onAnimateWater?.onEndAnimate()
        }
        return heightValueAnimator
    }

    fun getYLineWater(): Int{
        val location = IntArray(2)
        level_water.getLocationOnScreen(location)
        return location[1]
    }
}