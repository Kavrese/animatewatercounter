package com.wsr.water

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import com.wsr.water.WaterCounterView.TypeTextSize.Companion.isTypeTextSize
import kotlinx.android.synthetic.main.layout_water_counter_view.view.*

class WaterCounterView: LinearLayout {
    constructor(context: Context): this(context, null)

    constructor(context: Context, attr: AttributeSet): this(context, attr, 0)

    constructor(context: Context, attr: AttributeSet?, defStyle: Int = 0): super(context, attr, defStyle){
        val array = context.theme.obtainStyledAttributes(attr, R.styleable.WaterCounterView, defStyle, 0)

        if (array.hasValue(R.styleable.WaterCounterView_typeText))
            type = array.getInt(R.styleable.WaterCounterView_typeText, 0).isTypeTextSize()

        inflate(context, R.layout.layout_water_counter_view, this)
    }

    var count: Int = 0
    set(value) {
        if (value in minCount..maxCount) {
            field = value
            refreshCount()
        }
    }

    var title: String = "TextView"
    set(value) {
        field = value
        refreshTitle()
    }

    var type: TypeTextSize = TypeTextSize.BIG
    set(value) {
        field = value
        refresh()
    }

    var maxCount = 100
    var minCount = 0

    override fun onFinishInflate() {
        super.onFinishInflate()
        refresh()
    }

    private fun refresh(){
        refreshCount()
        refreshTitle()
    }

    private fun refreshTitle(){
        if (counter_title != null) {
            counter_title.text = title
            syncTypeTextAndTextView(counter_title)
        }
    }

    private fun refreshCount(){
        if (counter_text != null) {
            counter_text.text = resources.getString(R.string.count_text_template, count)
            syncTypeTextAndTextView(counter_text)
        }
    }

    private fun syncTypeTextAndTextView(textView: TextView){
        textView.apply {
            textSize = type.textSize
            val params = layoutParams
            params.height = type.heightViewDP.convertDpToPx(context).toInt()
            layoutParams = params
        }
    }

    fun checkCollisionCounterY(y: Int): Boolean{
        val location = IntArray(2)
        getLocationOnScreen(location)
        val heightTitle = counter_title.layoutParams.height
        val heightCounter = counter_text.layoutParams.height
        val height = heightTitle + heightCounter / 2
        val bottomY = height + location[1]
        return y <= bottomY
    }

    fun checkCollisionTitleY(y: Int): Boolean{
        val location = IntArray(2)
        getLocationOnScreen(location)
        val heightTitle = counter_title.layoutParams.height
        val bottomY = heightTitle / 2 + location[1]
        return y <= bottomY
    }

    fun setColorTextTitle(color: Int){
        counter_title.setTextColor(color)
    }

    fun setColorTextCounter(color: Int){
        counter_text.setTextColor(color)
    }

    enum class TypeTextSize(val textSize: Float, val heightViewDP: Float){
        BIG(36f, 50f),
        SMALL(22f, 30f);

        companion object {
            fun Int.isTypeTextSize(): TypeTextSize{
                return TypeTextSize.values()[this]
            }
        }
    }
}