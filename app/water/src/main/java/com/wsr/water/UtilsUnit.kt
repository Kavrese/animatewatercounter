package com.wsr.water

import android.content.Context
import android.util.DisplayMetrics


fun Float.convertDpToPx(context: Context): Float{
    return this * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun Float.convertPixelsToDp(context: Context): Float{
    return this / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}
