package com.wsr.water

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat

class GradientParser(context: Context, resId: Int) {
    private val gradient = (ContextCompat.getDrawable(context, resId) as BitmapDrawable).bitmap


    @RequiresApi(Build.VERSION_CODES.Q)
    fun parseColors(countParts: Int): List<Color>{
        val xs = calcXsColors(countParts)
        return getColorsPixelsGradient(xs)
    }

    private fun calcXsColors(count: Int): List<Int>{
        val sizeOnePart = gradient.width / count
        return (0 until count).map {
            it * sizeOnePart
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun getColorsPixelsGradient(xs: List<Int>): List<Color>{
        val y = gradient.height / 2
        return xs.map {  x ->
            gradient.getColor(x, y)
        }
    }
}