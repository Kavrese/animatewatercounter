package com.wsr.water

interface OnAnimateWater {
    fun onStartAnimate()
    fun onEndAnimate()
}